import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_file_exists(host):
    file1 = host.file("/usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js")
    file2 = host.file("/usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js.bak")
    assert file1.exists
    assert file2.exists


def test_service_runs(host):
    pveproxy = host.service("pveproxy")
    assert pveproxy.is_running
