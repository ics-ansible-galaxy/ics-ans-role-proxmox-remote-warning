# ics-ans-role-proxmox-remote-warning

Ansible role to install proxmox-remote-warning.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
# remove the subscription reminder
del_reminder: true

# path to the subscription reminder
proxmox_file: /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js

# remove the enterprise repository
del_enterprise_repo: false
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-proxmox-remote-warning
```

## License

BSD 2-clause
